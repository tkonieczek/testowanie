package testowanie.zadanie2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TriangleExecutor {

	public List<TestResult> exeTriangle(List<Test> tests) {
		List<TestResult> testsResults= new ArrayList<>();
		for (Test testData : tests) {
			testsResults.add(executeSingleTest(testData));
		}
		return testsResults; 
	}
	
	private TestResult executeSingleTest(Test test){
		TestResult testResult = new TestResult();
        try {
            Runtime rt = Runtime.getRuntime();
           // Process pr = rt.exec("java -jar D:\\Triangle.jar 2 3 4");
          //  Process pr = rt.exec("java -jar Triangle.jar 2 3 4");
            Process pr = rt.exec("java -jar Triangle.jar " + prepereArgumentsForTriangle(test.getInputSides()));

            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String triangleResult = input.readLine();
//            while((triangleResult = input.readLine()) != null) {
//                System.out.println(triangleResult);
//            }
            testResult.setResult(triangleResult);
            testResult.setExpectedResult(test.getExpectedResult());
            testResult.setInputSides(test.getInputSides());

            int exitVal = pr.waitFor();

        } catch(Exception e) {
            e.printStackTrace();
        }
        return testResult;		
	}
	
	private String prepereArgumentsForTriangle(Integer[] inputSides){
		String result = "";
		for (int i = 0; i < inputSides.length; i++) {
			result += inputSides[i].toString();
			if(i != inputSides.length - 1){
				result += " ";
			}
		}
		return result;
	}
}
