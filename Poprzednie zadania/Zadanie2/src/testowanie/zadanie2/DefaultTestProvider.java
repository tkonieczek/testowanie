package testowanie.zadanie2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class DefaultTestProvider implements TestProvider {

	@Override
	public List<Test> readTests() {
		String tests = readTestsFromFile();
		String[] lines = tests.split("\n");
		List<Test> result = new ArrayList<Test>();
		for (int i = 0; i < lines.length; i++) {
			Test testData = new Test();
			if (i % 2 == 0) {
				testData.setInputSides(parseInputData(lines[i]));
				testData.setExpectedResult(lines[i + 1]);
			} else {
				continue;
			}
			result.add(testData);
		}
		// printTestData(result);
		return result;
	}

	private String readTestsFromFile() {
		//File testsFile = new File("testy.txt");
		String tests;
		try {
			//tests = FileUtils.readFileToString(testsFile);
			tests = readFile("testy.txt", "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		return tests;
	}

	private void printTestData(List<Test> testsData) {
		for (Test testData : testsData) {
			System.out.print("Input data: ");
			for (Integer i : testData.getInputSides()) {
				System.out.print(i + " ");
			}
			System.out.print(", Expected result: "
					+ testData.getExpectedResult());

		}
	}

	private Integer[] parseInputData(String inputDataLine) {
		Integer[] result = new Integer[3];
		String[] inputDataAsStringArray = inputDataLine.trim().split(" ");
		for (int i = 0; i < inputDataAsStringArray.length; i++) {
			try {
				result[i] = Integer.parseInt(inputDataAsStringArray[i]);
			} catch (Exception e) {
				System.out.println("B�AD - nieprawid�owy format pliku wejsciowego");
			}

		}
		return result;
	}

	public String readFile(String file, String csName)
			throws IOException {
		Charset cs = Charset.forName(csName);
		return readFile(file, cs);
	}

	public String readFile(String file, Charset cs) throws IOException {
		FileInputStream stream = new FileInputStream(file);
		try {
			Reader reader = new BufferedReader(
					new InputStreamReader(stream, cs));
			StringBuilder builder = new StringBuilder();
			char[] buffer = new char[8192];
			int read;
			while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, read);
			}
			return builder.toString();
		} finally {
			stream.close();
		}
	}
}
