package testowanie.zadanie3.testy;

import testowanie.zadanie3.CarPanel;

public class CarPanelTest {
	
	CarPanel carPanel = new CarPanel();
	private static final int MAXDBL = 200;
	private static final int MINDBL = 6;
	private static final int METERS_IN_KILOMETER = 1000;
	private static final int SECOND_IN_HOUR = 3600;
	
	
   // w VM arguments ustawic -ea	
	public void runTests(){
		test(0, 0, 0 , 0);
		test(120, 1, 120 , 0);
		test(MAXDBL, 1, MAXDBL , 1);
		test(1, 0.01, 100 , 0);
		test(120.01, 1, 120.01 , 1);
		test(MINDBL, 2, MINDBL/2 , 0);
	}
	
	private void test(double s, double t, double oczekiwanaPrednosc, int oczekiwanyWarning){
		s *= METERS_IN_KILOMETER;
		t *= SECOND_IN_HOUR;

		carPanel.uaktualnijPomiary(s, t);
		carPanel.aktualizujPanel();
		System.out.println("Dane wejsciowe: s = " + s + "m, t = " + t + "s");
		System.out.println("Predkosc: " +  carPanel.getPredkosc() + ", predkosc oczekiwana: " + oczekiwanaPrednosc);
		System.out.println("WARN: " +  carPanel.getWarn() + ", oczekiwany WARN: " + oczekiwanyWarning);
		assert carPanel.getPredkosc() == oczekiwanaPrednosc : "Nieprawidlowa wartosc predkosci";
		assert carPanel.getWarn() == oczekiwanyWarning : "Nieprawidlowa wartosc WARN";
		System.out.println("Test przeszed�\n");
	}
}
