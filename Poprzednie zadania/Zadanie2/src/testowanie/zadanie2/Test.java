package testowanie.zadanie2;

public class Test {

	Integer[] inputSides;
	String expectedResult;

	public Integer[] getInputSides() {
		return inputSides;
	}

	public void setInputSides(Integer[] inputSides) {
		this.inputSides = inputSides;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

}
