package testowanie.zadanie3;

public class CarPanel {

	double s = 0;
	double t = 0;
	double predkosc;
	byte WARN = 0;

	public void uaktualnijPomiary(double s, double t) {
		this.s = s;
		this.t = t;
	}

	public void aktualizujPanel() {
		obliczPrednosc();
		uaktualnijKomunikatOPrzekroczeniuPrednosci();
		//wyswietlPanel();
	}
	
	public double getPredkosc(){
		return predkosc;
	}
	
	public int getWarn(){
		return WARN;
	}
	
	private void obliczPrednosc() {
		if (t == 0) {
			predkosc = 0;
		} else {
			predkosc = (s / 1000) / (t / 3600);
		}
	}

	private void uaktualnijKomunikatOPrzekroczeniuPrednosci() {
		if (predkosc > 120) {
			WARN = 1;
		} else if (WARN == 1 && predkosc < 120) {
			WARN = 0;
		}
	}

	private void wyswietlPanel() {
		System.out.println("Prednosc : " + Integer.toString((int) predkosc));
		if (WARN == 1) {
			System.out.println("Przekroczyles prednosc");
		}
	}
}
