﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadanie6;

namespace Zadanie6Testy
{
    [TestClass]
    public class TestCases01
    {
        private int SIDE_EQAL_DELTA = 0;

        [TestMethod]
        public void testCreatetion()
        {
            Triangle triangle = new Triangle(2, 4, 6);
            Assert.IsNotNull(triangle);
        }

        [TestMethod]
        public void testSides()
        {
            Triangle triangle = new Triangle(2, 4, 6);
            Assert.AreEqual(triangle.Side1, 2, SIDE_EQAL_DELTA);
            Assert.AreEqual(triangle.Side2, 4, SIDE_EQAL_DELTA);
            Assert.AreEqual(triangle.Side3, 6, SIDE_EQAL_DELTA);
        }

        [TestMethod]
        public void testIsRight()
        { // prostokatny
            Triangle triangle1 = new Triangle(3, 4, 5);
            Assert.IsTrue(triangle1.IsRightAngled());
            Triangle triangle2 = new Triangle(5, 12, 13);
            Assert.IsTrue(triangle2.IsRightAngled());
            Triangle triangle3 = new Triangle(7, 24, 25);
            Assert.IsTrue(triangle3.IsRightAngled());
            Triangle triangle4 = new Triangle(9, 40, 41);
            Assert.IsTrue(triangle4.IsRightAngled());
            Triangle triangle5 = new Triangle(11, 60, 61);
            Assert.IsTrue(triangle5.IsRightAngled());
        }

        [TestMethod]  // rownoramienny
        public void testIsIsosceles()
        {
            Triangle triangle1 = new Triangle(4, 4, 6);
            Assert.IsTrue(triangle1.IsIsosceles());
            Triangle triangle2 = new Triangle(7, 5, 5);
            Assert.IsTrue(triangle2.IsIsosceles());
            Triangle triangle3 = new Triangle(8, 3, 8);
            Assert.IsTrue(triangle3.IsIsosceles());
        }

        [TestMethod]  // rownoboczny
        public void testIsEquilateral()
        {
            Triangle triangle1 = new Triangle(2, 2, 2);
            Assert.IsTrue(triangle1.IsEquilateral());
            Triangle triangle2 = new Triangle(5, 5, 5);
            Assert.IsTrue(triangle2.IsEquilateral());
            Triangle triangle3 = new Triangle(1, 1, 1);
            Assert.IsTrue(triangle3.IsEquilateral());
            Triangle triangle4 = new Triangle(100, 100, 100);
            Assert.IsTrue(triangle4.IsEquilateral());
        }

    }
}
