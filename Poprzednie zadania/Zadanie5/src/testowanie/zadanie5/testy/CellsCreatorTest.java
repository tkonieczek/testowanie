package testowanie.zadanie5.testy;

import testowanie.zadanie5.CellsCreator;

public class CellsCreatorTest {

	CellsCreator cellsCreateor = new CellsCreator();
	
	public void runTests(){
		test("4", "7", 28);
		test("4.5", "7.4", 35);
		test("4.37", "7.55", 32);
		test("4b", "as7&*(", 28);
		test("5..2fsa", "4*(", 0);
	}
	
	private void test(String rows, String columns, int expectedCells){
		int cells = cellsCreateor.createCells(rows, columns);
		System.out.println("DANE WEJ�CIOWE: " + "wiersze = " + rows + ", komorki = " + columns);
		System.out.println("DANE WYJ�CIOWE: komorki = " + cells);
		System.out.println("DANE OCZEKIWANE: komorki = " + expectedCells);
		assert cells == expectedCells : "Nieprawidlowy wynik";
		System.out.println("Test przeszed�");
	}
}
