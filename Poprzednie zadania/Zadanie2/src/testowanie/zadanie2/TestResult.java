package testowanie.zadanie2;

public class TestResult {

	Integer[] inputSides;
	String expectedResult;
	String result;

	public Integer[] getInputSides() {
		return inputSides;
	}

	public void setInputSides(Integer[] inputSides) {
		this.inputSides = inputSides;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
