package testowanie.zadanie2;
import java.io.DataInputStream;
import java.io.IOException;

public class Triangle {

	private int side1, side2, side3;

	public Triangle(int s1, int s2, int s3) {
		side1 = s1;
		side2 = s2;
		side3 = s3;
	}

	public boolean is_triangle() {
		if (side1 > 0 && side2 > 0 && side3 > 0) {
			return side1 + side2 > side3 && side1 + side3 > side2
					&& side2 + side3 > side1;

		}
		return false;
	}

	public boolean is_right() {
		if (((side1 * side1) == ((side2 * side2) + (side3 * side3)))
				|| ((side2 * side2) == ((side1 * side1) + (side3 * side3)))
				|| ((side3 * side3) == ((side1 * side1) + (side2 * side2))))
			return true;
		else
			return false;
	}

	public boolean is_scalene() {
		if ((side1 != side2) && (side1 != side3) && (side2 != side3))
			return true;
		else
			return false;
	}

	public boolean is_isosceles() {
		if (((side1 == side2) && (side1 != side3))
				|| ((side1 == side3) && (side1 != side2))
				|| ((side2 == side3) && (side2 != side1)))
			return true;
		else
			return false;
	}

	public boolean is_equilateral() {
		if ((side1 == side2) && (side1 == side3))
			return true;
		else
			return false;
	}
}

class Test_Triangle {

	public static void main(String[] args) throws IOException {
		
		DataInputStream stdin = new DataInputStream(System.in);
		
		if (args.length == 3) {
			int site1 = Integer.parseInt(args[0]);
			int site2 = Integer.parseInt(args[1]);
			int site3 = Integer.parseInt(args[2]);
			Triangle tri = new Triangle(site1, site2, site3);

			int result = 0;
			if (!tri.is_triangle()) {
				result = 1;
			} else if (tri.is_right()) { // prostokatny
				result = 2;
			} else if (tri.is_scalene()) { // roznoboczny
				result = 3;
			} else if (tri.is_isosceles()) { // rownoramienny
				result = 4;
			} else if (tri.is_equilateral()) { // rownoboczny
				result = 5;
			}
			System.out.println(result);
		} else {
			System.out.println("Niprawidlowa liczba parametrow progamu");
		}
	}
}
