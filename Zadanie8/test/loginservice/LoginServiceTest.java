package loginservice;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LoginServiceTest {

	UserDataBase mockDb;
	Encoder encoder;
	LoginService loginService;
	
	@Before
	public void setUp(){
		mockDb = EasyMock.createMock(UserDataBase.class);
		encoder = EasyMock.createMock(Encoder.class);
		loginService = new LoginService(mockDb, encoder);
	}
	
	@After
	public void clean(){
		mockDb = null;
		encoder = null;
		loginService = null;
	}
	
	@Test //test gdy jest polaczenie z baza danych i jest uzytwkonik w bazie (mock)
	public void testLogin1(){
		String userName = "Tomek";
		String password = "zaq1";
		User user = new User("Tomek", "zaq1");
		String hashPassword = "qwertyuiop";
		boolean isConnected = true;
		
		EasyMock.expect(mockDb.isConnected()).andReturn(isConnected);
		EasyMock.expect(mockDb.getUserByLoginAndPassword(userName, hashPassword)).andReturn(user);
		EasyMock.replay(mockDb);
		
		EasyMock.expect(encoder.encode(password)).andReturn(hashPassword);
		EasyMock.replay(encoder);
		
		Assert.assertTrue(loginService.login(userName, password));
		EasyMock.verify(mockDb);
		EasyMock.verify(encoder);
	}
	
	@Test //test gdy jest polaczenie z baza danych i nie ma uzytwkonika w bazie
	public void testLogin2(){
		String userName = "Tomek";
		String password = "zaq1";
		User user = null;
		String hashPassword = "qwertyuiop";
		boolean isConnected = true;
		
		EasyMock.expect(mockDb.isConnected()).andReturn(isConnected);
		EasyMock.expect(mockDb.getUserByLoginAndPassword(userName, hashPassword)).andReturn(user);
		EasyMock.replay(mockDb);
		
		EasyMock.expect(encoder.encode(password)).andReturn(hashPassword);
		EasyMock.replay(encoder);
		
		Assert.assertFalse(loginService.login(userName, password));
		EasyMock.verify(mockDb);
		EasyMock.verify(encoder);
	}
	
	@Test //test gdy nie ma polaczenia z baza danych i jest uzytwkonik w bazie (mock)
	public void testLogin3(){
		String userName = "Tomek";
		String password = "zaq1";
		User user = new User("Tomek", "zaq1");
		String hashPassword = "qwertyuiop";
		boolean isConnected = false;
		
		EasyMock.expect(mockDb.isConnected()).andReturn(isConnected);
		mockDb.connect();
		EasyMock.expectLastCall();
		EasyMock.expect(mockDb.getUserByLoginAndPassword(userName, hashPassword)).andReturn(user);
		EasyMock.replay(mockDb);
		
		EasyMock.expect(encoder.encode(password)).andReturn(hashPassword);
		EasyMock.replay(encoder);
		
		Assert.assertTrue(loginService.login(userName, password));
		EasyMock.verify(mockDb);
		EasyMock.verify(encoder);
	}
	
	@Test //test gdy nie ma polaczenia z baza danych i nie ma uzytwkonika w bazie
	public void testLogin4(){
		String userName = "Tomek";
		String password = "zaq1";
		User user = null;
		String hashPassword = "qwertyuiop";
		boolean isConnected = false;
		
		EasyMock.expect(mockDb.isConnected()).andReturn(isConnected);
		mockDb.connect();
		EasyMock.expectLastCall();
		EasyMock.expect(mockDb.getUserByLoginAndPassword(userName, hashPassword)).andReturn(user);
		EasyMock.replay(mockDb);
		
		EasyMock.expect(encoder.encode(password)).andReturn(hashPassword);
		EasyMock.replay(encoder);
		
		Assert.assertFalse(loginService.login(userName, password));
		EasyMock.verify(mockDb);
		EasyMock.verify(encoder);
	}
	
}
