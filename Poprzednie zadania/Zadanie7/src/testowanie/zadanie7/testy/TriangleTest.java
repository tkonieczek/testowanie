package testowanie.zadanie7.testy;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import testowanie.zadanie7.Triangle;

public class TriangleTest {


//    @BeforeClass � metoda wykonywana raz przed wszystkimi testami,
//    @Before � metoda wykonywana przed ka�dym testem,
//    @Test � oznaczenie testu,
//    @After � metoda wykonywana po ka�dym te�cie,
//    @AfterClass �metoda wykonywana raz po wszystkich testach,
//    @Ignore � wy��czenie testu,

	private static int SIDE_EQAL_DELTA;
	
    @BeforeClass
    public static void initilization(){
    	SIDE_EQAL_DELTA = 0;
    }
    
	@Test
	public void testCreatetion() {
		Triangle triangle = new Triangle(2, 4, 6);
		Assert.assertNotNull("", triangle);
	}
	
	@Test
	public void testSides() {
		Triangle triangle = new Triangle(2, 4, 6);
		Assert.assertEquals(triangle.getSide1(), 2, SIDE_EQAL_DELTA);
		Assert.assertEquals(triangle.getSide2(), 4, SIDE_EQAL_DELTA);
		Assert.assertEquals(triangle.getSide3(), 6, SIDE_EQAL_DELTA);
	}
	
	@Test
	public void testIsTriangle() {
		Triangle triangle1 = new Triangle(2, 4, 5);
		Assert.assertTrue(triangle1.is_triangle());
		Triangle triangle2 = new Triangle(8, 2, 9);
		Assert.assertTrue(triangle2.is_triangle());
	}	
	
	@Test
	public void testIsNotTriangle() {
		Triangle triangle1 = new Triangle(1, 2, 3);
		Assert.assertFalse(triangle1.is_triangle());
		Triangle triangle2 = new Triangle(-4, 2, 3);
		Assert.assertFalse(triangle2.is_triangle());
		Triangle triangle3 = new Triangle(-3, -1, 7);
		Assert.assertFalse(triangle3.is_triangle());
		Triangle triangle4 = new Triangle(8, 1, 10);
		Assert.assertFalse(triangle4.is_triangle());
		Triangle triangle5 = new Triangle(0, 0, -5);
		Assert.assertFalse(triangle5.is_triangle());
		Triangle triangle6 = new Triangle(-1, -2, -3);
		Assert.assertFalse(triangle6.is_triangle());
		Triangle triangle7 = new Triangle(1, 3, 2);
		Assert.assertFalse(triangle7.is_triangle());
		Triangle triangle8 = new Triangle(2, 1, 3);
		Assert.assertFalse(triangle8.is_triangle());
		Triangle triangle9 = new Triangle(2, 3, 1);
		Assert.assertFalse(triangle9.is_triangle());
		Triangle triangle10 = new Triangle(3, 1, 2);
		Assert.assertFalse(triangle10.is_triangle());
		Triangle triangle11 = new Triangle(3, 2, 1);
		Assert.assertFalse(triangle11.is_triangle());
	}	
	
	@Test
	public void testIsRight() { // prostokatny
		Triangle triangle1 = new Triangle(3, 4, 5);
		Assert.assertTrue(triangle1.is_right());
		Triangle triangle2 = new Triangle(5, 12, 13);
		Assert.assertTrue(triangle2.is_right());
		Triangle triangle3 = new Triangle(7, 24, 25);
		Assert.assertTrue(triangle3.is_right());
		Triangle triangle4 = new Triangle(9, 40, 41);
		Assert.assertTrue(triangle4.is_right());
		Triangle triangle5 = new Triangle(11, 60, 61);
		Assert.assertTrue(triangle5.is_right());
	}	
	
	@Test  // rownoramienny
	public void testIsIsosceles() {
		Triangle triangle1 = new Triangle(4, 4, 6);
		Assert.assertTrue(triangle1.is_isosceles());
		Triangle triangle2 = new Triangle(7, 5, 5);
		Assert.assertTrue(triangle2.is_isosceles());
		Triangle triangle3 = new Triangle(8, 3, 8);
		Assert.assertTrue(triangle3.is_isosceles());
	}	
	
	@Test  // rownoboczny
	public void testIsEquilateral() {
		Triangle triangle1 = new Triangle(2, 2, 2);
		Assert.assertTrue(triangle1.is_equilateral());
		Triangle triangle2 = new Triangle(5, 5, 5);
		Assert.assertTrue(triangle2.is_equilateral());
		Triangle triangle3 = new Triangle(1, 1, 1);
		Assert.assertTrue(triangle3.is_equilateral());
		Triangle triangle4 = new Triangle(100, 100, 100);
		Assert.assertTrue(triangle4.is_equilateral());
	}	

}
