package testowanie.zadanie2;

import java.util.List;

public interface TestProvider {
	
	List<Test> readTests();
}
