package testowanie.zadanie5;

public class CellsCreator {

	private static final int MAX = 75;

	public int createCells(String numberOfRows, String numberOfColumns) {
		double rows = 0;
		double columns = 0;
		try {
			rows = Double.parseDouble(removeNoNumber(numberOfRows));
			columns = Double.parseDouble(removeNoNumber(numberOfColumns));			
		} catch (NumberFormatException e) {
			System.out.println("Niepoprawne wartosci");
		}

		int rowAsInt = (int) Math.round(rows);
		int colsAsInt = (int) Math.round(columns);
		if (rowAsInt > MAX) {
			rowAsInt = MAX;
		}
		if (colsAsInt > MAX) {
			colsAsInt = MAX;
		}
		return rowAsInt * colsAsInt;
	}

	private String removeNoNumber(String number) {
		char[] numberAsCharArray = number.toCharArray();
		StringBuilder result = new StringBuilder();
		for (char c : numberAsCharArray) {
			if (Character.isDigit(c) || c == '.') {
				result.append(c);
			}
		}
		return result.toString();
	}
}
