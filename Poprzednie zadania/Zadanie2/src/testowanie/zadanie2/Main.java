package testowanie.zadanie2;

import java.util.List;


public class Main {

	public static void main(String[] args){
		TestProvider testProvider = new DefaultTestProvider();
		TriangleExecutor triangleExecutor  = new TriangleExecutor();
		List<TestResult> testsResults = triangleExecutor.exeTriangle(testProvider.readTests());
		System.out.println("Liczba testow: " + testsResults.size());
		for (TestResult testResult : testsResults) {
			System.out.print("input data: ");
			for (Integer i : testResult.getInputSides()) {
				System.out.print(i + " ");
			}
			System.out.print(", result: " + testResult.getResult());
			System.out.print(", expected result: " + testResult.getExpectedResult());
			System.out.println("");
		}
	}
	
}
