package testowanie.zadanie4.testy;

import testowanie.zadanie4.Bankier;
import testowanie.zadanie4.Klient;
import testowanie.zadanie4.Oferta;


public class BankierTest {
	
	Bankier bankier = new Bankier();
	
   // w VM arguments ustawic -ea	
	public void runTests(){
		Klient klient = new Klient();
		Oferta oczekiwanaOferta = new Oferta();
		
		klient.setDochod(6000);
		klient.setPosiadaKarte(true);
		klient.setSkladaWniosek(true);
		oczekiwanaOferta.setOfertaKarty(false);
		oczekiwanaOferta.setOfertaKredytu(false);
		oczekiwanaOferta.setPrzyznanieKredytu(true);
		test(klient, oczekiwanaOferta);
		
		klient.setDochod(3500);
		klient.setPosiadaKarte(true);
		klient.setSkladaWniosek(false);
		oczekiwanaOferta.setOfertaKarty(false);
		oczekiwanaOferta.setOfertaKredytu(true);
		oczekiwanaOferta.setPrzyznanieKredytu(false);
		test(klient, oczekiwanaOferta);
		
		klient.setDochod(5000.01);
		klient.setPosiadaKarte(false);
		klient.setSkladaWniosek(true);
		oczekiwanaOferta.setOfertaKarty(true);
		oczekiwanaOferta.setOfertaKredytu(true);
		oczekiwanaOferta.setPrzyznanieKredytu(true);
		test(klient, oczekiwanaOferta);		
		
		klient.setDochod(12832);
		klient.setPosiadaKarte(false);
		klient.setSkladaWniosek(false);
		oczekiwanaOferta.setOfertaKarty(true);
		oczekiwanaOferta.setOfertaKredytu(true);
		oczekiwanaOferta.setPrzyznanieKredytu(false);
		test(klient, oczekiwanaOferta);		
		
		klient.setDochod(2004.22);
		klient.setPosiadaKarte(true);
		klient.setSkladaWniosek(true);
		oczekiwanaOferta.setOfertaKarty(false);
		oczekiwanaOferta.setOfertaKredytu(false);
		oczekiwanaOferta.setPrzyznanieKredytu(false);
		test(klient, oczekiwanaOferta);	
		
		klient.setDochod(0);
		klient.setPosiadaKarte(false);
		klient.setSkladaWniosek(false);
		oczekiwanaOferta.setOfertaKarty(false);
		oczekiwanaOferta.setOfertaKredytu(true);
		oczekiwanaOferta.setPrzyznanieKredytu(false);
		test(klient, oczekiwanaOferta);	
	}
	
	private void test(Klient klient, Oferta oczekiwanaOferta){
		Oferta oferta = bankier.obsluzKlienta(klient);
		System.out.print("DANE WEJ�CIOWE: dochod Klienta: " + klient.getDochod());
		System.out.print(", czy posiada kart� " + (klient.posiadanieKarty() ? "TAK" : "NIE"));
		System.out.println(", czy z�o�y� wniosek " + (klient.getWniosekOKredyt() ? "TAK" : "NIE") + ", ");
		
		System.out.print("DANE WYJ�CIOWE: oferty karty: " + (oferta.ofertaKarty() ? "TAK" : "NIE") + ", ");
		System.out.print("oferty kredytu: " + (oferta.ofertaKredytu() ? "TAK" : "NIE") + ", ");
		System.out.println("przyznanie kredytu: " + (oferta.przyznanieKredytu() ? "TAK" : "NIE") + ", ");
		
		System.out.print("OCZEKIWANE DANE WYJ�CIOWE: oferta karty: " + (oczekiwanaOferta.ofertaKarty() ? "TAK" : "NIE") + ", ");
		System.out.print("oferta kredytu: " + (oczekiwanaOferta.ofertaKredytu() ? "TAK" : "NIE") + ", ");
		System.out.println("przyznanie kredytu: " + (oczekiwanaOferta.przyznanieKredytu() ? "TAK" : "NIE") + ", ");		
		System.out.println("");
		assert oferta.ofertaKarty() == oczekiwanaOferta.ofertaKarty() : "Nieporawna oferta karty";
		assert oferta.ofertaKredytu() == oczekiwanaOferta.ofertaKredytu() : "Nieporawna oferta kredytu";
		assert oferta.przyznanieKredytu() == oczekiwanaOferta.przyznanieKredytu() : "Nieporawne przyznanie kredytu";
		System.out.println("Test przeszed�");
	}
}
