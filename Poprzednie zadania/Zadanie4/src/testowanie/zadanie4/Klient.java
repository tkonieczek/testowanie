package testowanie.zadanie4;

public class Klient {

	private double dochod;
	private boolean posiadaKarte;
	private boolean skladaWniosek;

	public double getDochod() {
		return dochod;
	}

	public void setDochod(double dochod) {
		this.dochod = dochod;
	}

	public boolean posiadanieKarty() {
		return posiadaKarte;
	}

	public void setPosiadaKarte(boolean posiadaKarte) {
		this.posiadaKarte = posiadaKarte;
	}

	public boolean getWniosekOKredyt() {
		return skladaWniosek;
	}

	public void setSkladaWniosek(boolean skladaWniosek) {
		this.skladaWniosek = skladaWniosek;
	}

}
