package testowanie.zadanie4;

public class Oferta {

	private boolean ofertaKarty;
	private boolean ofertaKredytu;
	private boolean przyznanieKredytu;

	public boolean ofertaKarty() {
		return ofertaKarty;
	}

	public void setOfertaKarty(boolean ofertaKarty) {
		this.ofertaKarty = ofertaKarty;
	}

	public boolean ofertaKredytu() {
		return ofertaKredytu;
	}

	public void setOfertaKredytu(boolean ofertaKredytu) {
		this.ofertaKredytu = ofertaKredytu;
	}

	public boolean przyznanieKredytu() {
		return przyznanieKredytu;
	}

	public void setPrzyznanieKredytu(boolean przyznanieKredytu) {
		this.przyznanieKredytu = przyznanieKredytu;
	}
}
