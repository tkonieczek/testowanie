package testowanie.zadanie4;

public class Bankier {

	private double PROG_KREDYTOWY = 5000;

	public Oferta obsluzKlienta(Klient klient) {
		Oferta oferta = new Oferta();
		if (klient.getDochod() > PROG_KREDYTOWY) {
			if (!klient.posiadanieKarty()) {
				oferta.setOfertaKarty(true);
			} else {
				oferta.setOfertaKarty(false);
			}
			
			if (klient.posiadanieKarty() && klient.getWniosekOKredyt()) {
				oferta.setOfertaKredytu(false);
			} else {
				oferta.setOfertaKredytu(true);
			}
			
			if (klient.getWniosekOKredyt()) {
				oferta.setPrzyznanieKredytu(true);
			} else {
				oferta.setPrzyznanieKredytu(false);
			}
		} else {
			oferta.setOfertaKarty(false);
			oferta.setOfertaKarty(false);
			if (klient.getWniosekOKredyt()) {
				oferta.setOfertaKredytu(false);
			} else {
				oferta.setOfertaKredytu(true);
			}
		}
		return oferta;
	}

}
